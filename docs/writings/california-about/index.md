# About

*Written 2014-09-27*

Hi there! Welcome to my little corner of the internet!

My name is Nicholas. I'm a BS/MS student of Computer Engineering at
Drexel University. In my first two years, I've won my way up the ranks,
first as a Bookseller in the
[University Bookstore](https://drexel.bncollege.com/shop/drexel/home)
then as an entitled Desk Jockey for the Engineering Advising Center,
a Research Minion for our
[MET-Lab](https://drexel.edu/excite/discovery/met-lab/), and finally
a priveleged SysAdmin for the 
[Computer Science Department](https://drexel.edu/cci/academics/computer-science-department/).

I'm a Pittsburgher, born and raised, but I now live on the West Coast
as an Apple Intern. I started this blog mostly as a way to archive my
travels and keep interested family and friends in the loop, but you are
more than welcome to read it too!

I hope you enjoy it, and I wish you many, many Safe Travels!

-- Nicholas, 2014-09-27