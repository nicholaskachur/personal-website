# Writings

Here lies a collection of the writings I have made for personal websites,
past and present, which I hope to expand over time.


## Opinionated Musings

Writings here might be charitably considered attempts to think out loud,
so take them with a grain of salt as I learn how to articulate and
develop an argument.

- [A War on Hate](./hate/)

## California Adventure

When I was a pre-junior at Drexel, I embarked on a California adventure
as an intern at Apple in Cupertino. These are the articles I wrote
during that time, presented in reverse-chronological order.

- [Shopping in Suburbia](./shopping-in-suburbia/)
- [Donuts, Drivers, and the City](./donuts-drivers-city/)
- [Welcome to the West Coast](./welcome-west-coast/)
- [Striking out West](./striking-out-west/)
- [About](./california-about/) 
