# Striking out West

*Written 2014-09-18*

The journey was grueling: two flights, two hours of delays, and two
trips through security.

My flight out of Pittsburgh was fine, I had time to find my gate, relax,
and enjoy the lunch my Father had packed--always eager to show off his
sandwich making skills.

![Pittsburgh Airport Terminal](./pittsburgh-terminal.jpg)

_Goodbye Pittsburgh! Public Domain by
[John Marino](https://commons.wikimedia.org/wiki/File:KPIT_Land_Terminal_Night.jpg)_

**It was Dallas where things started to go awry.**

The flight out of Pittsburgh departed late, so we barely made it to
Dallas in time to catch the connecting flight. I was fidgety throughout
the whole deboarding process, but thankfully made it out with about 20
minutes until my next departure time, which was only a few gates away.

I rushed out of the plane, looking for gate C20. There was C18, C19, but
no C20. I didn't see it, the closest thing was a sign for "Baggage
Carousel C20". I was desparate at this point, so I followed the sign.

**Rookie mistake.**

Suddenly I was outside the security zone; now I was getting frantic. I
ran around to the nearest security point, and couldn't even see it
because the line wrapped around the corner. I snapped at my phone to
check my flight details. _Delayed, thank goodness!_

I was never so glad to have my flight delayed, but now I had to resign
myself to spending most of that delay back in the security line. It was
fun, let me tell you.

**Skipping ahead,** I landed in San Jose around 8pm, two-and-a-half
hours later than I'd planned. By now it was completely dark outside,
fully nighttime. Thankfully there were plenty of taxis, and I found a
full minivan to myself, my bags, and my guitar. The night air felt fresh
and crisp after more than 10 hours in airports and planes, and the ride
went swiftly. Now I just had to find my way home.

After bumbling about for more than 20 minutes, a friendly security guard
pointed me towards the apartment buildings, though he didn't know which
one was which. I took a stab in the dark, and guessed rightly. Up some
stairs, into an elevator, and down the hall.

**I was, finally, home.**