<style>
img {
    padding: 1em;
    margin-left: auto;
    margin-right: auto;
    display: block;
}
</style>

# Projects

To view the primary home of my personal source, see
[my GitLab Profile](https://gitlab.com/nickkachur). It hasn't seen much
love lately, but it's the single largest repository of my code that is
open and available to the public. A summary of some choice projects
follows.

**Table of Contents**

[[toc]]


## Kachur.me

_Fall 2014 &ndash; Present_

I've had this current domain since 2014, when GitHub gave out one-year
of free .me domains and DigitalOcean hosting to registered students.
Since then I have run the Ghost and WordPress blogging platforms on it,
before transitioning to a 
[handwritten HTML and CSS implementation](https://gitlab.com/nickkachur/personal-website-old),
and now the current
[VuePress implementation](https://gitlab.com/nickkachur/personal-website)
running on the [Vercel platform](https://vercel.com). It has afforded me
a reliable playground and a domain to use for personal email, so I'm
grateful to GitHub for getting me started down this road.

<img src="./Kachur.me-Homepage-20200621-min.png"
  alt="Kachur.me homepage as of 2020-06-21." width="512" />

_Kachur.me homepage as of 2020-06-21._

## Orthodox Philly

_November 2019 &ndash; Present_

When I was first deciding to move to Philadelphia, one of the factors
was where I would go to Church, and how good of a parish I could find. I
was fortunate to already know the pastor of one of the suburban
parishes, but before that I stumbled upon 
[OrthodoxPhilly.org](https://orthodoxphilly.org), which purported to
help with this exploration. When I found it, the website had been
recently updated, but after a while the original maintainers moved away
and moved on, so it sat dormant for many moons.

Remembering how much I enjoyed there being a website dedicated to the
Philadelphia Orthodox Community, I reached out to a few people to see
about taking it over, and was met with a resounding yes! Naturally, it
lay unmaintained a while longer as we slowly transferred ownership of
the domain, social media handles, and WordPress source, but finally it
happened, and a while after that I went live with
[a new version](https://orthodoxphilly.org) hosted from
[Squarespace](https://squarespace.com) on Saturday June 20th, 2020. Of
course, this wasn't without hiccups, as Squarespace didn't update the
SSL certificate right away, but hopefully this will be easier to
maintain and involve outside contributions from organizations like the
local Clergy Brotherhood.

<img src="./OrthodoxPhilly-Homepage-20200621-min.png"
  alt="Refreshed Orthodox Philly homepage as of 2020-06-21."
  width="640" />

_Orthodox Philly homepage as of 2020-06-21._


## StreetSavvy

_September 2016 &ndash; June 2018_


StreetSavvy was a senior design project for my Bachelor's in Computer
Engineering at Drexel University. It was an attempt to visualize safety
in a geographic region. To do this, my team and I collected Police and
Fire calls for service from San Francisco's Open Civic Data project,
normalized them, stored them in a database server, processed them, and
presented them through a custom-built API, web application, and iOS
application.

I helped set up and run the team's modified Agile Scrum, built out the
server infrastructure, and developed the iOS application based off my
teammates' API.

The website is no longer live, but source can be seen at the
[StreetSavvy GitLab](https://gitlab.com/streetsavvy). As I have time,
I'd like to rebuild the project, but for now it is complete.

<img src="./streetsavvy-ios-screenshot.png"
  alt="StreetSavvy iOS application displaying safety heatmap and routing."
  width="192" />

_Screenshot of the StreetSavvy iOS application displaying safety heatmap
and routing._

## KP-POP

_February 2016 &ndash; March 2016_

During my sophomore year, I took my first intensive programming course
at Drexel, Advanced Programming Techniques. Largely a UNIX course, this
was made easier by my use of remote Linux programming during my first
co-op. We used a book which I quickly grew to love, Kernighan and Pike's
_The Practice of Programming_. I enjoyed the assigned exercises, and
later decided to try to work through the book on my own. Thus far I have
worked up through the second chapter, but my main pride and joy of this
project is that it comes complete with a simple CMake build system,
making building and running the exercises simple and reliable.

It is available at [the GitLab project](https://gitlab.com/nickkachur/kp-pop)

<img src="./kp-pop-cover.jpg"
  alt="The cover of Kernighan and Pike's venerable Practice of Programming."
  width="259" />

_Kernighan and Pike's Practice of Programming cover image._