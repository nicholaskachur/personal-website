# License

![Creative Commons Attribution 4.0 International License](https://i.creativecommons.org/l/by/4.0/88x31.png)

Except where otherwise noted, the content of this repository is licensed
under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
