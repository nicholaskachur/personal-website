# Kachur.me

This is the current source of my personal website, https://kachur.me,
built with [VuePress](https://vuepress.vuejs.org) and hosted on
[Vercel](https://vercel.com).

## License

![Creative Commons Attribution 4.0 International License](https://i.creativecommons.org/l/by/4.0/88x31.png)

Except where otherwise noted, the content of this repository is licensed
under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
